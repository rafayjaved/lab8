OBJS = main.o vector.o matrix.o

prog : $(OBJS)
	g++ -std=c++0x -o prog $(OBJS)
		   
vector.o : vector.cc vector.h
	g++ -std=c++0x -c vector.cc

matrix.o : matrix.cc matrix.h
	g++ -std=c++0x -c matrix.cc

main.o : main.cc vector.h matrix.h
	g++ -pthread -std=c++0x -c main.cc

clean :
	rm $(OBJS)
